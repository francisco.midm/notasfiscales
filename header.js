import * as React from 'react';
import { Button, View,Text  } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';




export default function Header() {
    const navigation = useNavigation();
    return (
      <View 
        style={{
        height:60,
        backgroundColor:'#2C2C2C',
        width:'100%',
        paddingHorizontal:30,
        marginTop:25}}>
            <View style={{flexDirection:'row'}} >
              
              <View style={{flex:10,paddingTop:15}}>
                <Text style={{color:"#ffffff",fontSize:20}}>Notas Fiscales</Text>
              </View>

              <View style={{flex:1,paddingTop:15,marginRight:5}}>
                <Icon
                  name ="bars"
                  color="#ff0000"
                  size={30}
                  onPress={() => navigation.openDrawer()}
              />
              </View>
            
            
            </View>
               

      </View>
    );
  }

