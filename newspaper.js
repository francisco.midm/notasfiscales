import * as React from 'react';
import { Button, View, Image, Text } from 'react-native';
import Header from './header';




export default function Newspaper({ navigation }) {
    return (
      <View >
        <Header></Header>
        <View style={{padding:10}}>
            <View>
              <Text style={{color:'#ff0000',fontSize:40,marginBottom:20}}>Noticias</Text>
            </View>
            <View style={{  alignItems: 'center', marginBottom:10 }}>
              <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2020/05/SAT-Notas-Fiscales-1200x480.jpg'}}
                style={{width:'100%', height: 300}} />
            </View>
            <View>
              <Text style={{color:'#a4a4a4',fontSize:12,textAlign: 'left',marginRight:0}}> Notas Fiscales at  mayo 29, 2020</Text>
            </View>
            <View>
              <Text style={{padding:0,fontSize:20,fontWeight: "bold"}}>Proporción 80/20 en la enajenación de inmuebles por parte de las personas físicas</Text>
            </View>
              <Text>
              Cuando una persona física enajena un inmueble, para determinar la ganancia objeto del Impuesto Sobre la Renta, puede descontar, del ingreso obtenido, el costo del terreno...
              </Text>
            <View>

            </View>
            
          </View>
        </View>
      
    );
  }

