import * as React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import newspaper from './newspaper'
import books from './books'
import magazine from './magazine'
import mystore from './mystore'


function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="newspaper">
        <Drawer.Screen name="Noticias" component={newspaper} />
        <Drawer.Screen name="Libros" component={books} />
        <Drawer.Screen name="Revistas" component={magazine} />
        <Drawer.Screen name="Mis compras" component={mystore} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

