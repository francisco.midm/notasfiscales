import * as React from 'react';
import { Button, View,Image, Text } from 'react-native';
import Header from './header';

export default function Magazine({ navigation }) {
    return (
      <View >
        <Header></Header>
        <View style={{padding:10}}>
            <View>
              <Text style={{color:'#ff0000',fontSize:40,marginBottom:20}}>Revistas</Text>
            </View>
            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2020/05/Portada-NF-producto.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>NOTAS FISCALES 294 (mayo 2020) (Digital)</Text>
                <Text style={{color:'#ff0000'}}>$110.00</Text>
              </View>
            </View>


            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2020/03/Portada-NF-producto.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>NOTAS FISCALES 293 (abril 2020) (Digital)</Text>
                <Text style={{color:'#ff0000'}}>$110.00</Text>
              </View>
            </View>

            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2020/02/Portada-NF-producto-1.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>NOTAS FISCALES 292 (marzo 2020) (Digital)</Text>
                <Text style={{color:'#ff0000'}}>$110.00</Text>
              </View>
            </View>

            
           
            
          </View>
        </View>
    );
  }

