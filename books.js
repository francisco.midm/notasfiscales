import * as React from 'react';
import { Button, View,Image, Text } from 'react-native';
import Header from './header';

export default function Books({ navigation }) {
    return (
      <View >
        <Header></Header>
        <View style={{padding:10}}>
            <View>
              <Text style={{color:'#ff0000',fontSize:40,marginBottom:20}}>Libros</Text>
            </View>
            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2020/02/Libro-ACTUALIZACION-FISCAL-2020.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>Actualización Fiscal 2020</Text>
                <Text style={{color:'#ff0000'}}>$490.00</Text>
              </View>
            </View>


            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2018/05/Nominas-6a-edic.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>Manual de Nóminas 6ª edición</Text>
                <Text style={{color:'#ff0000'}}>$980.00</Text>
              </View>
            </View>

            <View style={{flexDirection:'row',marginBottom:20}}>
              <View style={{flex:4}}>
                  <Image source={{uri: 'https://notasfiscales.com.mx/wp-content/uploads/2018/02/fiscal-2018-1.png'}}
                    style={{width:'100%', height: 200}} />
              </View>
              <View style={{flex:5,marginLeft:10}}>
                <Text style={{fontSize:20}}>Actualización Fiscal 2018</Text>
                <Text style={{color:'#ff0000'}}>$470.00</Text>
              </View>
            </View>

            
           
            
          </View>
        </View>
    );
  }

